﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuJobs.Constants
{
    public class Atom
    {
        public const string Entry = "entry";
        public const string Id = "id";
        public const string Author = "author";
        public const string Content = "content";
        public const string Published = "published";
        public const string Title = "title";
        public const string Link = "link";
        public const string Href = "href";
        public const string Updated = "updated";
    }
}
