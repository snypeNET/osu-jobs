﻿using System;

namespace OsuJobs.Constants
{
    public class ExceptionResources
    {
        public const string XmlDocumentNull = "XML document is null, unable to map";
        public const string ParameterNullOrUnknown = "Parameter {0} is null or unknown";
    }
}
