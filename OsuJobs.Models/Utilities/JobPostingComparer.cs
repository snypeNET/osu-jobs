﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsuJobs.Models.Utilities
{
    public class JobPostingComparer : IEqualityComparer<JobPosting>
    {
        public bool Equals(JobPosting x, JobPosting y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(JobPosting obj)
        {
            unchecked //suppresses integer overflow checking 
            {
                int hash = 31;
                hash = hash * 22 + obj.Id.GetHashCode();
                hash = hash * 22 + (obj.Title ?? string.Empty).GetHashCode();
                hash = hash * 22 + (obj.Author ?? string.Empty).GetHashCode();
                return hash;
            }
        }
    }
}
