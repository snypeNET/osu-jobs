﻿using System;

namespace OsuJobs.Models
{
    public class JobPosting
    {
        /// <summary>
        /// Job posting unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Date the job posting was published
        /// </summary>
        public DateTime Published { get; set; }

        /// <summary>
        /// Date this job posting was last updated
        /// </summary>
        public DateTime? LastUpdated { get; set; }

        /// <summary>
        /// Returns a formatted date as MM/dd/yyyy HH:MM:ss am or pm
        /// if there is no value then it will return a blank
        /// string
        /// </summary>
        public string LastUpdatedFormatted
        {
            get
            {
                string formattedDate = string.Empty;

                if (this.LastUpdated.HasValue)
                {
                    formattedDate = this.LastUpdated.Value.ToString();
                }

                return formattedDate;
            }
        }

        /// <summary>
        /// Returns a formatted date as MM/dd/yyyy HH:MM:ss am or pm
        /// if there is no value then it will return a blank
        /// string
        /// </summary>
        public string PublishedFormatted
        {
            get
            {
                return this.Published.ToString();
            }
        }

        /// <summary>
        /// Url to actual job posting
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Title of the job posting
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Main content and description of job posting
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Writer of job posting
        /// </summary>
        public string Author { get; set; }
    }
}
