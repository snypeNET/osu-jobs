﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OsuJobs.Models.Data;

namespace OsuJobs.Models
{
    public class JobFilter
    {
        public JobProperty Type { get; set; }

        public string Value { get; set; }
    }
}
