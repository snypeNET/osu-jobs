﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OsuJobs.Models.Data;

namespace OsuJobs.Models
{
    public class JobSort
    {
        /// <summary>
        /// sort by property of the job posting object
        /// </summary>
        public JobProperty SortBy { get; set;}

        /// <summary>
        /// direction to sort (ascending or descending)
        /// </summary>
        public DirectionSort Direction { get; set; }
    }
}
