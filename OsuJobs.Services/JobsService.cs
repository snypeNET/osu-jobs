﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;

using OsuJobs.Models;
using OsuJobs.Mapping;

namespace OsuJobs.Services
{
    public class JobsService
    {
        private JobPostingMapper jobMapper;

        public JobsService()
        {
            jobMapper = new JobPostingMapper();
        }

        /// <summary>
        /// Retrieves list of jobs from Usa Jobs ATOM feed
        /// </summary>
        /// <returns></returns>
        public IEnumerable<JobPosting> GetJobs()
        {
            try
            {
                var document = XDocument.Load(DataSources.AtomFeedUrl);
                return this.jobMapper.Convert(document);
            }
            catch(Exception e)
            {
                return new List<JobPosting>();
            }
        }
    }
}
