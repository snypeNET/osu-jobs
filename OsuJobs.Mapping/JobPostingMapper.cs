﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Web;

using OsuJobs.Models;
using OsuJobs.Constants;

namespace OsuJobs.Mapping
{
    public class JobPostingMapper :
        IMapper<XDocument, IEnumerable<JobPosting>>
    {
        /// <summary>
        /// Convert XDocument to an Enumberable of JobPosting
        /// </summary>
        /// <param name="source">XML Document</param>
        /// <returns>Enumerable of JobPosting, will never return NULL</returns>
        public IEnumerable<JobPosting> Convert(XDocument source)
        {
            if (source == null)
            {
                throw new Exception(ExceptionResources.XmlDocumentNull);
            }

            var jobs = new List<JobPosting>();
            
            //you must pass in the namespace associated to each element 
            //otherwise nothing is found 
            string nameSpace = source.Root.GetDefaultNamespace().NamespaceName; 
            var xName = XName.Get(Atom.Entry, nameSpace);
            var jobEntries = source.Descendants(xName);

            //Don't bother looping if nothing is found
            if (jobEntries != null && jobEntries.Any())
            {
                //loop through job entries in XML
                foreach (var entry in jobEntries)
                {
                    var posting = new JobPosting()
                    {
                        Id = entry.Element(XName.Get(Atom.Id, nameSpace)).Value,
                        Author = entry.Element(XName.Get(Atom.Author, nameSpace)).Value,
                        Content = HttpUtility.HtmlDecode(entry.Element(XName.Get(Atom.Content, nameSpace)).Value),
                        Published = DateTime.Parse(entry.Element(XName.Get(Atom.Published, nameSpace)).Value),
                        Title = entry.Element(XName.Get(Atom.Title, nameSpace)).Value,
                        Url = entry.Element(XName.Get(Atom.Link, nameSpace)).Attribute(Atom.Href).Value
                    };

                    if (entry.Element(XName.Get(Atom.Updated, nameSpace)).Value != null)
                    {
                        posting.LastUpdated = DateTime.Parse(entry.Element(XName.Get(Atom.Updated, nameSpace)).Value);
                    }

                    jobs.Add(posting);
                }
            }

            return jobs;
        }
    }
}
