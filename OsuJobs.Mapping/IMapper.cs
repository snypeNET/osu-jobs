﻿using System;

namespace OsuJobs.Mapping
{
    interface IMapper<Tin, Tout>
    {
        Tout Convert(Tin source);
    }
}
