﻿/// <reference path="jquery-1.10.2.min.js" />
/// <reference path="jquery.scrollTo.min.js" />

$(function(){
    $(window).scroll(function () {
        if ($(window).scrollTop() > $(window).height()) {
            $(window).trigger("toolbarNotVisible"); //raise event for whatever is listening
        } else {
            $(window).trigger("toolbarVisible"); //raise event for whatever is listening
        }
    });

    $(window).bind("toolbarNotVisible", function () {
        $("#scrollToTop").fadeIn(200);
    }).bind("toolbarVisible", function(){
        $("#scrollToTop").fadeOut(200);
    });

    $("#scrollToTop").click(function () {
        $(window).scrollTo(".title", 1000, {
            offset: { top: -50, left: 0 }
        });
    });
});
