﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using OsuJobs.Services;
using OsuJobs.Constants;
using OsuJobs.Models;
using OsuJobs.Models.Data;
using OsuJobs.Models.Utilities;

namespace OsuJobs.Controllers
{
    public class JobsController : Controller
    {
        private readonly JobsService jobService= new JobsService();

        /// <summary>
        /// Retreives the postings from the
        /// job postings service and returns them to the 
        /// partial view _JobPostings
        /// </summary>
        /// <returns></returns>
        public ActionResult Postings()
        {
            var postings = this.jobService.GetJobs().OrderBy(x => x.Title);
            this.Session[SessionKeys.JobPostings] = postings;

            return this.PartialView("_JobPostings", postings);
        }
        /// <summary>
        /// Takes in a list of filters and runs them against the postings in session
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult FilterJobs(List<JobFilter> filter)
        {
            var jobs = this.Session[SessionKeys.JobPostings] as IEnumerable<JobPosting>;

            //if for some reason the session cache is empty, e.g. session timeout,
            //pull down another copy of the jobs and cache it in session
            if (jobs == null)
            {
                jobs = this.jobService.GetJobs();
                this.Session[SessionKeys.JobPostings] = jobs;
            }

            var filteredJobs = new List<JobPosting>();

            //filter results
            this.FilterJobs(filteredJobs, jobs, filter);

            return this.PartialView("_Postings", filteredJobs.Distinct(new JobPostingComparer()).OrderBy(x => x.Title));
        }

        /// <summary>
        /// Sorts the job postings based on the sort on property and direction
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public ActionResult SortJobs(List<JobFilter> filter, JobSort sort)
        {
            var jobs = this.Session[SessionKeys.JobPostings] as IEnumerable<JobPosting>;

            //if for some reason the session cache is empty, e.g. session timeout,
            //pull down another copy of the jobs and cache it in session
            if (jobs == null)
            {
                jobs = this.jobService.GetJobs();
                this.Session[SessionKeys.JobPostings] = jobs;
            }

            var filteredJobs = new List<JobPosting>();

            this.FilterJobs(filteredJobs, jobs, filter);

            ViewBag.SortBy = sort;

            switch (sort.SortBy)
            {
                case JobProperty.Department:
                    if (sort.Direction == DirectionSort.Asc)
                    {
                        return this.PartialView("_JustPostings", filteredJobs.Distinct(new JobPostingComparer()).OrderBy(x => x.Author));
                    }
                    else
                    {
                        return this.PartialView("_JustPostings", filteredJobs.Distinct(new JobPostingComparer()).OrderByDescending(x => x.Author));
                    }
                case JobProperty.Title:
                    if (sort.Direction == DirectionSort.Asc)
                    {
                        return this.PartialView("_JustPostings", filteredJobs.Distinct(new JobPostingComparer()).OrderBy(x => x.Title));
                    }
                    else
                    {
                        return this.PartialView("_JustPostings", filteredJobs.Distinct(new JobPostingComparer()).OrderByDescending(x => x.Title));
                    }
                default:
                    throw new Exception(string.Format(ExceptionResources.ParameterNullOrUnknown, "Sort By"));
            }
        }

        /// <summary>
        /// Takes in referenced list for the results and the orignial data along with 
        /// a list of filters to apply to the original data 
        /// </summary>
        /// <param name="filteredJobs"></param>
        /// <param name="jobs"></param>
        /// <param name="filter"></param>
        private void FilterJobs(List<JobPosting> filteredJobs, IEnumerable<JobPosting> jobs, List<JobFilter> filter)
        {
            if (filter == null || !filter.Any())
            {
                //if nothing is selected then return everything
                filteredJobs.AddRange(jobs);
            }
            else
            {
                filter.ForEach(delegate(JobFilter filtering)
                {
                    //Author should equal department
                    if (filtering.Type == JobProperty.Title)
                    {
                        filteredJobs.AddRange(jobs.Where(x => x.Title.Equals(filtering.Value, StringComparison.InvariantCultureIgnoreCase)));
                    }
                    else
                    {
                        filteredJobs.AddRange(jobs.Where(x => x.Author.Equals(filtering.Value, StringComparison.InvariantCultureIgnoreCase)));
                    }
                });
            }
        }

        /// <summary>
        /// Retrieves the remaining details from the session cache, if it exists,
        /// and binds it to a details view for javascript to display
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult JobDetails(string id)
        {
            var jobs = this.Session[SessionKeys.JobPostings] as IEnumerable<JobPosting>;

            //if for some reason the session cache is empty, e.g. session timeout,
            //pull down another copy of the jobs and cache it in session
            if (jobs == null)
            {
                jobs = this.jobService.GetJobs();
                this.Session[SessionKeys.JobPostings] = jobs;
            }

            //check to make sure the job still exists if 
            //a new set was pulled down
            if(!jobs.Any(x => x.Id == id))
            {
                return this.PartialView("_DetailsNotFound");
            }

            return this.PartialView("_Details", jobs.Single(x => x.Id == id));
        }

    }
}
